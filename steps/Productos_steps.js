var { Given, When, After, Then } = require('cucumber');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;

After(function (scenarioResult) {
    let self = this;

   // if (scenarioResult.result.status === 'failed') {
    return browser.takeScreenshot().then(function (screenshot) {
        const decodedImage = new Buffer(screenshot.replace(/^data:image\/png;base64,/, ''), 'base64');
        self.attach(decodedImage, 'image/png');
        console.log("scenarioResult: " + scenarioResult.result.status);
    });
    //}
});

Given(/^usuario entra al sitio$/, function () {
    return browser.get("http://localhost:4200/");
});

When(/^usuario da click en el boton eliminar$/, function () {
    return element(by.xpath("//products-root/app-product-list/div[3]/div/table/tbody/tr[1]/td[9]/button/span")).click();
});

Then(/^usuario da click en el boton Cancelar$/, function () {
    return element(by.xpath("/html/body/div/div/div[3]/button[2]"));
});