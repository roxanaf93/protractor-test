exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    getPageTimeout: 60000,
    allScriptsTimeout: 50000,
    capabilities: {
      'browserName' : 'chrome',
      chromeOptions: {
      args: [ "--headless", "--disable-gpu", "--window-size=800,600" ]
    }
    },
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: [
      'features/*.feature'
    ],
    cucumberOpts: {
      require: 'steps/*_steps.js',
      format: ['json:.tmp/results.json'],
      tags: ''
    },
    onPrepare: function () {
      browser.manage().window().maximize();
    },
    plugins: [{
      package: require.resolve('protractor-multiple-cucumber-html-reporter-plugin'),
      options: {
        automaticallyGenerateReport: true,
        removeExistingJsonReportFile: true,
        customData: {
          title: 'Run info',
          data: [
            { label: 'Project', value: 'Protractor Project' },
            { label: 'Release', value: '1.0' },
            { label: 'Cycle', value: 'B11221.34321' }
          ]
        }
      }
    }]
   }
   